import pytest
import typing as tp

from src.helper import preprocess_input


class TestPreprocessInput:
    @pytest.mark.parametrize(
        "input_str,expected_output",
        [
            (None, None),
            ("", ""),
            ("CaPiTal LETTERS", "capital letters"),
            ("hello (world)", "hello "),
            ("hello [world]", "hello "),
            (r"cleaned symbols:()[]{}#°.,;'«»", "cleaned symbols"),
            ('"', ''),
            ("1+1", r"1\+1"),
        ],
    )
    def test_preprocess_input_correct(self, input_str: tp.Optional[str], expected_output: tp.Optional[str]) -> None:
        assert preprocess_input(input_str) == expected_output

    def test_raises_when_incorrect_type(self) -> None:
        with pytest.raises(ValueError):
            preprocess_input(123)
