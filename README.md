recsysart parser
==============================

Posters parser based on the Kinopoisk's API for the MTS Kion dataset.<br>
Searches for links to posters via the API using film titles and year of release: 
* Parse poster's URL along with kinopoisk, kinopoisk HD and IMDB IDs into CSV file
* Download poster images
* Upload result CSV table to PostgreSQL
* Upload images to Minio S3-like storage

## Project set up

```
conda create -n parser python=3.8.13
conda activate parser
pip3 install poetry==1.5.1
poetry install --with dev
```

After that create `.env` file from `.env.example` and fill it.
The Kinopoisk API can be obtained from the [KinopoiskDEV bot](https://t.me/kinopoiskdev_bot)

## Project usage
### Full DVC pipeline
Run DVC pipeline:<br>
* To prepare the input data for the parser: `dvc repro preprocess`
* To run full pipeline after URL collecting: `dvc repro`

Warning: Parsing itself is not a part of DVC pipleine! Check details below

### URL parsing
As the parsing capability may be limited by API quota, URL pasring was taken out of the DVC pipeline.

`python3 scripts/parse_url.py [arguments]`<br>
Arguments:
* Required:
    * `--start-idx` - start slice index of the csv file
    * `--end-idx` - end slice index of the csv file
* Pre-defined:
    * `--items-input` - input item csv. By default it's `data/interim/items_clean.csv`
    * `--items-output-folder` - folder for output files. By default it's `"data/processed/parsed_chunks"`. Output file will named by template `item_parsed_<start-idx>_<end-idx>.csv`.

Example: <br>
`python3 scripts/parse_url.py --start-idx 0 --end-idx 1000`

## Development
The following tools are available:
* Run code fomatters: `make`
* Run linters: `make lint`
* Run unit tests: `make test`

## Docs

Kinopoisk API Swagger: <br>
https://api.kinopoisk.dev/v1/documentation

## License
MIT
