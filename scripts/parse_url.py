import logging
import os
import typing as tp

import click
import pandas as pd
from tqdm import tqdm

from src.helper import get_first_non_null, replace_e_from_end, trim_last_word
from src.url_parser import KinopoiskParser

tqdm.pandas()


def iterate_responses_by_titles(
    sort_field: str,
    titles: tp.List[str],
    year: tp.Union[tp.Optional[int], tp.Optional[str]],
) -> tp.Optional[tp.Dict]:
    parser = KinopoiskParser()
    for title in titles:
        if sort_field == "name":
            info_dict = parser.get_film_info(
                sort_field=sort_field,
                year=year,
                name=title,
            )
        elif sort_field == "alternativeName":
            info_dict = parser.get_film_info(
                sort_field=sort_field,
                year=year,
                alternativeName=title,
            )
        else:
            raise ValueError("search_field should be `name` or `alternativeName`")

        if info_dict is not None and info_dict != {}:
            return info_dict
    return None


def create_titles_variants(title: str) -> tp.List[str]:
    if not isinstance(title, str):
        raise TypeError(f"Incorrect non-string input type: {title}:{type(title)}")
    result = list([title])
    trimmed_title = trim_last_word(title)
    if title != trimmed_title:
        result.append(trimmed_title)
    if "е" in title:
        result.append(replace_e_from_end(title))
    return result


def iterate_responses(
    title: str, original_title: tp.Optional[str], year: tp.Optional[int]
) -> tp.Optional[tp.Dict]:
    # Set up titles search combinations
    title_search_options = create_titles_variants(title)
    if original_title != r"none" and original_title is not None:
        title_orig_search_options = create_titles_variants(original_title)
    else:
        title_orig_search_options = []

    # Iterate over combinations
    if year != 0 and year is not None:
        # Query with selected release year
        info_ru = iterate_responses_by_titles("name", title_search_options, year)
        info_orig = iterate_responses_by_titles(
            "alternativeName", title_orig_search_options, year
        )
        info = get_first_non_null([info_ru, info_orig])
        if info is not None:
            return info

        # Query with +-2 release year range
        info_ru = iterate_responses_by_titles(
            "name",
            title_search_options,
            f"{year - 2}-{year + 2}",
        )
        info_orig = iterate_responses_by_titles(
            "alternativeName",
            title_orig_search_options,
            f"{year - 2}-{year + 2}",
        )
        info = get_first_non_null([info_ru, info_orig])
        if info is not None:
            return info

        # Query without release year
        info_ru = iterate_responses_by_titles("name", title_search_options, None)
        info_orig = iterate_responses_by_titles(
            "alternativeName", title_orig_search_options, None
        )
        info = get_first_non_null([info_ru, info_orig])
        if info is not None:
            return info
    # If we don't have release year for item
    else:
        info_ru = iterate_responses_by_titles("name", title_search_options, None)
        info_orig = iterate_responses_by_titles(
            "alternativeName", title_orig_search_options, None
        )
        info = get_first_non_null([info_ru, info_orig])
        if info is not None:
            return info
    # If all options failed
    return None


@click.command()
@click.option(
    "-i",
    "--items-input",
    "items_input_path",
    type=click.Path(),
    default="data/interim/items_clean.csv",
)
@click.option(
    "-o",
    "--items-output-folder",
    "items_output_folder",
    type=click.Path(),
    default="data/processed/parsed_chunks",
)
@click.option(
    "-s",
    "--start-idx",
    "start_index",
    type=int,
    default=0,
)
@click.option(
    "-e",
    "--end-idx",
    "end_index",
    type=int,
    default=10,
)
def parse_urls(
    items_input_path: str,
    items_output_folder: str,
    start_index: int,
    end_index: int,
) -> None:
    logging.basicConfig(level=logging.INFO)
    logging.info("Parse Kinopoisk URLs")
    logging.info(f"iloc range: {start_index}:{end_index}")

    items_df = pd.read_csv(
        items_input_path,
        dtype={
            "item_id": int,
            "content_type": str,
            "title": str,
            "title_orig": str,
            "release_year": int,
            "title_prep": str,
            "title_orig_perp": str,
        },
    )
    items_df = items_df.iloc[start_index:end_index, :]
    result = items_df.progress_apply(
        lambda x: iterate_responses(x.title_prep, x.title_orig_prep, x.release_year),
        axis=1,
    )
    res = result.apply(pd.Series)
    res = items_df.join(res)
    res.to_csv(
        os.path.join(items_output_folder, f"item_parsed_{start_index}_{end_index}.csv"),
        index=False,
    )


if __name__ == "__main__":
    parse_urls()
