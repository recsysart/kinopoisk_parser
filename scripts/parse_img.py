import logging
import os

import click
import pandas as pd
import requests
from requests.exceptions import ConnectionError
from tqdm import tqdm


def download_img(url: str, kion_id: int, folder: str):
    logging.basicConfig(level=logging.INFO)
    try:
        response = requests.get(url)
    except ConnectionError as e:
        logging.info(f"Kion item ID: {kion_id}")
        logging.info(e)
        return
    if response.status_code == 200:
        saving_path = os.path.join(folder, f"{kion_id}.jpg")
        with open(saving_path, "wb") as f:
            f.write(response.content)
    else:
        logging.info(f"Kion item ID: {kion_id}")
        logging.info(f"Response status code: {response.status_code}")


@click.command()
@click.argument("items_input_path")
@click.argument("output_folder_path")
def download_posters_for_dataset(
    items_input_path: str, output_folder_path: str
) -> None:
    items_df = pd.read_csv(items_input_path, usecols=["item_id", "poster.url"])
    items_df = items_df.rename(columns={"item_id": "id", "poster.url": "url"})
    items_df = items_df.dropna(subset="url")

    # Create output folder for images if not exist
    os.makedirs(output_folder_path, exist_ok=True)

    for _, row in tqdm(items_df.iterrows(), total=items_df.shape[0]):
        download_img(row["url"], row["id"], output_folder_path)


if __name__ == "__main__":
    download_posters_for_dataset()
