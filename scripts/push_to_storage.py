import os
from pathlib import Path

import click
from dotenv import load_dotenv
from minio import Minio
from minio.error import S3Error
from tqdm import tqdm

load_dotenv()


# Create client connection
minio_client = Minio(
    endpoint=os.getenv("MINIO_ENDPOINT"),
    access_key=os.getenv("MINIO_ACCESS_KEY"),
    secret_key=os.getenv("MINIO_SECRET_KEY"),
    secure=False,
)


def list_bucket_objects():
    objects = minio_client.list_objects(os.getenv("MINIO_BACKET"), recursive=True)
    file_names = []
    for obj in objects:
        file_names.append(obj.object_name)
    return file_names


def upload_image(image_path, bucket_name):
    _, file_name = os.path.split(image_path)
    try:
        # Check if the bucket already exists, and create it if it doesn't
        if not minio_client.bucket_exists(bucket_name):
            minio_client.make_bucket(bucket_name)

        # Upload the image file to the specified bucket and object name
        minio_client.fput_object(bucket_name, file_name, image_path)
    except S3Error as err:
        print(f"Error uploading the image: {err}")


def get_file_paths(folder_path):
    file_paths = []
    for root, _, files in os.walk(folder_path):
        for file in files:
            file_path = Path(os.path.join(root, file))
            file_paths.append(file_path)
    return file_paths


@click.command()
@click.option(
    "-p",
    "--path",
    "file_or_folder_path",
    type=click.Path(),
    default="data/images/",
)
@click.option(
    "-s",
    "--skip-existed",
    "skip_existed",
    type=bool,
    default=True,
)
def push_files_to_storage(file_or_folder_path: str, skip_existed: bool) -> None:
    # Push file
    if Path(file_or_folder_path).is_file():
        upload_image(
            file_or_folder_path,
            os.getenv("MINIO_BACKET"),
        )
        return
    # Push several files from folder
    existed_files = set(list_bucket_objects())
    for path in tqdm(get_file_paths(file_or_folder_path)):
        if skip_existed and path.name not in existed_files:
            upload_image(
                path,
                os.getenv("MINIO_BACKET"),
            )
        elif not skip_existed:
            upload_image(
                path,
                os.getenv("MINIO_BACKET"),
            )


if __name__ == "__main__":
    push_files_to_storage()
