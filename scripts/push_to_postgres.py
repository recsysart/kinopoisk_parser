import logging
import os

import click
import pandas as pd
from dotenv import load_dotenv
from sqlalchemy import create_engine
from sqlalchemy.engine import Engine
from sqlalchemy_utils import create_database, database_exists

load_dotenv()


def create_engine_to_feature_db(ensure_db_exists: bool = False) -> Engine:
    engine = create_engine(
        f'postgresql://{os.getenv("POSTGRES_USER")}:{os.getenv("POSTGRES_PASSWORD")}'
        f'@{os.getenv("SERVICES_HOST")}:5432/{os.getenv("POSTGRES_FEATURE_DB")}'
    )
    if ensure_db_exists and not database_exists(engine.url):
        create_database(engine.url)
        logging.info("No database detected. Database was created.")

    return engine


@click.command()
@click.option(
    "--csv-path",
    type=click.Path(exists=True),
    default="data/processed/full_item_parsed.csv",
    help="The file path to the CSV file containing the data to replace the table with.",
)
def replace_postgres_table_with_csv(csv_path):
    """
    Replace the contents of a PostgreSQL table with the data from a CSV file.

    Args:
    - csv_path (str): The file path to the CSV file containing the data to replace the table with.

    Returns:
    - None
    """
    logging.basicConfig(level=logging.INFO)

    df = pd.read_csv(csv_path)
    engine = create_engine_to_feature_db(ensure_db_exists=True)

    df.to_sql(
        name=os.getenv("POSTGRES_POSTERS_TABLE"),
        con=engine,
        chunksize=100,
        if_exists="replace",
        method="multi",
    )

    logging.info("Successfully updated PostreSQL table.")

    engine.dispose()


if __name__ == "__main__":
    replace_postgres_table_with_csv()
