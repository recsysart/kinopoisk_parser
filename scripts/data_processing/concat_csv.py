import os

import click
import pandas as pd


@click.command()
@click.argument('input_folder')
@click.argument('output_file')
def combine_csv_files(input_folder, output_file):
    """
    Combines all CSV files from the specified folder into a single CSV file.

    Arguments:
    - input_folder: The path to the folder containing CSV files.
    - output_file: The path to the output CSV file.
    """
    csv_files = [file for file in os.listdir(input_folder) if file.endswith('.csv')]

    dfs = []
    for file in csv_files:
        file_path = os.path.join(input_folder, file)
        tmp_df = pd.read_csv(file_path)
        dfs.append(tmp_df)

    result_df = pd.concat(dfs, ignore_index=True)
    result_df = result_df.drop_duplicates()

    result_df.to_csv(output_file, index=False)
    print(f"CSV files successfully combined into {output_file}.")


if __name__ == '__main__':
    combine_csv_files()
