import logging

import click
import pandas as pd

from src.helper import preprocess_input


@click.command()
@click.option(
    "-i",
    "--items-input",
    "items_input_path",
    type=click.Path(),
    default="data/raw/items.csv",
)
@click.option(
    "-n",
    "--interactions-input",
    "interactions_input_path",
    type=click.Path(),
    default="data/raw/interactions.csv",
)
@click.option(
    "-o",
    "--items-output",
    "items_output_path",
    type=click.Path(),
    default="data/interim/items_clean.csv",
)
def preprocess_raw_items_data(
    items_input_path: str, interactions_input_path: str, items_output_path: str
) -> None:
    logging.basicConfig(level=logging.INFO)
    logging.info("Preprocessing items raw data")

    items_df = pd.read_csv(
        items_input_path,
        usecols=["item_id", "content_type", "title", "title_orig", "release_year"],
    )
    # Note: NaNs only in `title_orig` and `release_year` columns
    # Note: `content_type` has only two classes: `film` and `series`
    # Handle NaNs
    items_df["title_orig"] = items_df["title_orig"].fillna(r"none")
    items_df["release_year"] = items_df["release_year"].fillna(0).astype("int")
    # Preproces titles
    items_df["title_prep"] = items_df["title"].apply(lambda x: preprocess_input(x))
    items_df["title_orig_prep"] = items_df["title_orig"].apply(
        lambda x: preprocess_input(x)
    )

    # Sort items by total intercations amount
    interactions_df = pd.read_csv(
        interactions_input_path, usecols=["item_id", "user_id"]
    )
    items_popularity = (
        interactions_df.groupby("item_id")["user_id"]
        .count()
        .sort_values(ascending=False)
        .reset_index()
        .rename(columns={"user_id": "inter_amount"})
    )
    items_df = items_popularity.merge(items_df, on="item_id", how="outer")

    items_df.to_csv(items_output_path, index=False)


if __name__ == "__main__":
    preprocess_raw_items_data()
