#! /bin/bash

echo "Downloading data."

wget -q https://storage.yandexcloud.net/datasouls-ods/materials/f90231b6/items.csv -O "data/raw/items.csv"
wget -q https://storage.yandexcloud.net/datasouls-ods/materials/04adaecc/interactions.csv -O "data/raw/interactions.csv"
