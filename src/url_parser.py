import os
import typing as tp

import flatdict
import requests
from dotenv import load_dotenv
from pydantic import BaseModel, ValidationError, parse_obj_as
from requests.exceptions import ConnectionError, ConnectTimeout, ReadTimeout

from src.model import KinopoiskResponse

load_dotenv()


class KinopoiskParser:
    def __init__(self):
        self.api_key = str(os.getenv("KINOPOISK_API_KEY"))
        self.search_params = None

    def _send_request(self) -> tp.Optional[tp.List[BaseModel]]:
        api_url = "https://api.kinopoisk.dev/v1.3/movie"
        params = {
            "page": "1",
            "limit": "3",
            "selectFields": [
                "id",
                "name",
                "alternativeName",
                "type",
                "poster",
                "externalId",
            ],
        }
        params.update(self.search_params)
        headers = {
            "accept": "application/json",
            "X-API-KEY": self.api_key,
        }
        try:
            resp = requests.get(api_url, params=params, headers=headers, timeout=300)
        except (ConnectTimeout, ReadTimeout) as e:
            print(f"API request timed out: {e}")
            return None
        except ConnectionError as e:
            print(f"Connection error: {e}")
            return None

        if resp.status_code == 200:
            resp_json = resp.json()
        else:
            print(f"Response status code: {resp.status_code}")
            print(f"{params}")
            return None

        # Convert to pydantic model
        try:
            response = parse_obj_as(KinopoiskResponse, resp_json)
        except ValidationError as e:
            print(resp_json)
            print(f"Pydantic validation error: {e}")

        docs = response.docs
        # Select docs only with URLs
        result = []
        if docs:
            result = [doc for doc in docs if doc.get_url()]
        else:
            return None

        if not result:
            return None
        return result

    def _sort_responses(
        self, sort_field: str, docs: tp.Optional[BaseModel]
    ) -> tp.Optional[tp.Dict]:
        if sort_field not in ["name", "alternativeName"]:
            raise ValueError("Sort field should be `name` or `alternativeName`")
        if not docs:
            return None
        reference = self.search_params[sort_field]
        hypotheses = []
        for doc in docs:
            name = doc.dict()[sort_field]
            similarity = self.get_word_match_ratio(reference, name)
            hypotheses.append((similarity, doc))
        # Select best matched doc
        hypotheses = sorted(hypotheses, key=lambda x: x[0], reverse=True)
        return hypotheses[0][1].dict()

    @staticmethod
    def get_word_match_ratio(
        reference: tp.Optional[str], hypothesis: tp.Optional[str]
    ) -> tp.Optional[float]:
        if not reference or not hypothesis:
            return None
        if not isinstance(reference, str) or not isinstance(hypothesis, str):
            raise TypeError(
                f"Incompatible input type: {reference}:{type(reference)}, {hypothesis}:{type(hypothesis)}"
            )
        words1 = reference.lower().split(" ")
        words2 = hypothesis.lower().split(" ")

        common_words = set(words1) & set(words2)
        common_word_count = len(common_words)
        words2_count = len(words2)

        if words2_count == 0:
            normalized_value = 0.0
        else:
            normalized_value = common_word_count / words2_count
        return normalized_value

    def get_film_info(self, sort_field: str, **search_params):
        self.search_params = search_params
        docs = self._send_request()
        dict_info = self._sort_responses(sort_field, docs)
        return flatdict.FlatDict(dict_info, ".")


if __name__ == "__main__":
    parser = KinopoiskParser()
    print(
        parser.get_film_info(sort_field="name", name="человек паук", year="2010-2023")
    )
