import typing as tp

from pydantic import BaseModel


class PosterURL(BaseModel):
    url: tp.Optional[str] = None
    previewUrl: tp.Optional[str] = None


class ExternalId(BaseModel):
    kpHD: tp.Optional[str] = None
    imdb: tp.Optional[str] = None
    tmdb: tp.Optional[str] = None


class KinopoiskMovieInfo(BaseModel):
    id: tp.Optional[int] = None
    name: tp.Optional[str] = None
    alternativeName: tp.Optional[str] = None
    type: tp.Optional[str] = None
    poster: tp.Optional[PosterURL] = None
    externalId: tp.Optional[ExternalId] = None

    def get_url(self):
        if self.poster and self.poster.url:
            return self.poster.url
        else:
            return None


class KinopoiskResponse(BaseModel):
    docs: tp.List[KinopoiskMovieInfo]
