import re
import typing as tp

PUNCT_LIST = [
    "(",
    ")",
    "[",
    "]",
    "{",
    "}",
    "#",
    "°",
    ".",
    ",",
    ":",
    ";",
    "'",
    '"',
    "«",
    "»",
]


def preprocess_input(text: tp.Optional[str]) -> tp.Optional[str]:
    if text is None:
        return None
    if not isinstance(text, str):
        raise ValueError(f"Incorrect input type: {text}, {type(text)}")
    # Remove text inside () and []
    title = re.sub(r"([(\[]).*?([)\]])", r"\1\2", text)
    # Remove punctuation
    target_punct_list = PUNCT_LIST
    pattern = "[" + re.escape("".join(target_punct_list)) + "]"
    title = re.sub(pattern, "", title)
    # Escape + sign
    title = title.replace("+", r"\+")
    # lower text
    title = title.lower()  # kinopoisk API doesn't depend on uppercase/lowercase
    return title


def trim_last_word(text: tp.Optional[str], max_trim_len=2) -> tp.Optional[str]:
    if text is None:
        return None
    if not isinstance(text, str):
        raise TypeError(f"Incorrect non-string input type: {text}:{type(text)}")
    split = text.rsplit(" ", 1)
    if len(split) < 2:
        return text
    trimmed_text, appendix = split
    if len(appendix) > max_trim_len:
        return text
    return trimmed_text


def replace_e_from_end(text: tp.Optional[str]) -> tp.Optional[str]:
    if text is None:
        return None
    if not isinstance(text, str):
        raise TypeError(f"Incorrect non-string input type: {text}:{type(text)}")
    split = text.rsplit("е", 1)
    return "ё".join(split)


def get_first_non_null(items: tp.List) -> tp.Optional[tp.Any]:
    item = next((item for item in items if item is not None), None)
    return item
